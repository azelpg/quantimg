# quantimg

http://azsky2.html.xdomain.jp/

画像を 2〜256 色に減色して、出力します。<br>
指定色を透過色とすることもできます。

**対応フォーマット**:<br>
BMP/PNG/GIF/WebP/JPEG (JPEG は入力のみ)

## 動作環境

Linux、macOS ほか

## コンパイル/インストール

ninja、pkg-config コマンドが必要です。<br>
各開発用ファイルのパッケージも必要になります。ReadMe を参照してください。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~
