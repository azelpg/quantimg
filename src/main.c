/*$
 Copyright (c) 2021-2022 Azel

 This file is part of quantimg.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/********************************
 * main
 ********************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "mlk.h"
#include "mlk_str.h"
#include "mlk_charset.h"
#include "mlk_imagebuf.h"

#include "def.h"
#include "quant.h"


//------------------

AppData g_app;

#define WEBP_MAXSIZE  16383

int app_get_options(int argc,char **argv);

//image.c
mImageBuf2 *image_load(const char *filename);
int image_after_load(mImageBuf2 *img,int tpcol);
mlkerr image_savefile(mImageBuf2 *img,const char *filename);

//quant.c
void quant_free(Quant *p);
Quant *quant_create(void);
mlkerr quant_run(Quant *p,mImageBuf2 *img);

//colspace.c
ColorSpace *colorspace_create(void);

//------------------


/* 初期化 */

static void _init_app(void)
{
	mMemset0(&g_app, sizeof(AppData));

	//初期値

	g_app.colnum = 256;
	g_app.dither_type = DITHER_ERR;
	g_app.dither_level = 2;
	g_app.opt_tpcol = -1;
	g_app.fputmes = 1;

	//確保

	g_app.quant = quant_create();
	g_app.cs = colorspace_create();
	g_app.palbuf = (uint8_t *)mMalloc(256 * 4);

	if(!g_app.quant || !g_app.cs || !g_app.palbuf)
		puterr_exit("memory alloc error");
}

/* mlkerr のエラー表示 */

static void _put_mlkerr(mlkerr n)
{
	switch(n)
	{
		case MLKERR_ALLOC:
			putmes("memory alloc error\n");
			break;
		default:
			putmes("error\n");
			break;
	}
}

/* 経過表示開始 */

static void _start_progress(int max)
{
	int i;

	if(!g_app.fputmes) return;

	g_app.prog_curcnt = 0;
	g_app.prog_curmark = 0;

	//各マーク時のカウント値
	// 0=10%, 9=100%

	for(i = 0; i < 10; i++)
		g_app.prog_tbl[i] = max * (i + 1) / 10;
}

/* 画像出力 */

static mlkerr _save_image(mImageBuf2 *img,mStr *fname)
{
	mStr str = MSTR_INIT;
	const char *ext[] = {
		"png", "bmp", "gif","webp"
	};
	mlkerr ret;

	//出力ファイル名

	if(mStrIsEmpty(&g_app.str_output))
	{
		//-o 指定なし: "[name]_q.[ext]"

		mStrPathGetBasename_noext(&str, fname->buf);

		mStrAppendText(&str, "_q.");
		mStrAppendText(&str, ext[g_app.out_format - 1]);
	}
	else if(g_app.is_output_dir)
	{
		//-o ディレクトリ指定

		mStrPathGetOutputFile(&str, fname->buf,
			g_app.str_output.buf, ext[g_app.out_format - 1]);
	}
	else
	{
		//-o ファイル名指定

		mStrCopy(&str, &g_app.str_output);
	}

	//出力

	ret = image_savefile(img, str.buf);

	mStrFree(&str);

	return ret;
}

/* ファイル処理 */

static void _proc_file(mStr *fname)
{
	mImageBuf2 *img;
	Quant *q = g_app.quant;
	mlkerr ret;

	//画像読み込み
	//(エラーは関数内で出力される)

	img = image_load(fname->buf);
	if(!img) return;

	//WebP 出力の場合、16383 px まで

	if(g_app.out_format == OUTFORMAT_WEBP
		&& (img->width > WEBP_MAXSIZE || img->height > WEBP_MAXSIZE))
	{
		putmes("WebP limited to 16383px\n");
		
		mImageBuf2_free(img);
		return;
	}

	//画像に透過色指定がない場合、オプションの透過色をセット

	if(g_app.tpcol == -1)
		g_app.tpcol = g_app.opt_tpcol;

	//入力画像処理

	g_app.is_img_have_tp = image_after_load(img, g_app.tpcol);

	//進捗初期化
	// :各処理の比率を同じにするため、画像高さを基準にする
	// :[輪郭抽出] [色の抽出] [減色:色の作成] [画像変換] [画像保存]

	_start_progress(img->height * 5);

	//減色処理

	ret = quant_run(g_app.quant, img);
	if(ret) goto ERR;

	//画像保存

	ret = _save_image(img, fname);
	if(ret) goto ERR;

	//

	mImageBuf2_free(img);

	//

	putmes(": ok\n");

	putmes("  color:%d -> %d", q->full_used_colnum, q->lastpalnum);

	if(!q->frun_quant)
		putmes("*");

	if(g_app.is_img_have_tp)
		putmes(", tp <#%06X>", g_app.tpcol);

	putmes("\n");

	return;

ERR:
	_put_mlkerr(ret);
	
	mImageBuf2_free(img);
	exit_app(1);
}

/** main */

int main(int argc,char **argv)
{
	mStr str = MSTR_INIT;
	int i;

	mInitLocale();
	
	_init_app();

	i = app_get_options(argc, argv);

	//

	for(; i < argc; i++)
	{
		//ロケール文字列のまま出力
		putmes("%s: ", argv[i]);

		//UTF-8 文字列
		mStrSetText_locale(&str, argv[i], -1);

		_proc_file(&str);
	}

	mStrFree(&str);

	//

	exit_app(0);

	return 0;
}


//======================
// 共通関数
//======================


/** アプリ終了
 *
 * ret: 戻り値 */

void exit_app(int ret)
{
	quant_free(g_app.quant);
	
	mFree(g_app.cs);
	mFree(g_app.palbuf);

	mStrFree(&g_app.str_output);

	exit(ret);
}

/** stdout に通常メッセージ表示
 *
 * 終端に改行は出力されない。 */

void putmes(const char *fmt,...)
{
	va_list ap;

	if(g_app.fputmes)
	{
		va_start(ap, fmt);
		vprintf(fmt, ap);
		va_end(ap);

		fflush(stdout);
	}
}

/** stdout にエラーを表示して、エラー終了させる
 *
 * 終端には改行が出力される。 */

void puterr_exit(const char *fmt,...)
{
	va_list ap;

	if(g_app.fputmes)
	{
		va_start(ap, fmt);
		vprintf(fmt, ap);
		putchar('\n');
		va_end(ap);
	}

	exit_app(1);
}

/** 進捗カウンタを加算
 *
 * val: 指定数加算。-1 で offpos を使う。
 * offpos: prog_offset の位置からの相対値 */

void progress_add(int val,int offpos)
{
	int i,cnt;

	if(!g_app.fputmes) return;

	//位置

	if(val < 0)
		g_app.prog_curcnt = g_app.prog_offset + offpos;
	else
		g_app.prog_curcnt += val;

	//マーク位置を超えていたら表示

	cnt = g_app.prog_curcnt;

	for(i = g_app.prog_curmark; i < 10; i++)
	{
		if(cnt < g_app.prog_tbl[i]) break;

		putchar('*');
	}

	//現在のマーク位置

	if(i != g_app.prog_curmark)
	{
		g_app.prog_curmark = i;

		fflush(stdout);
	}
}

