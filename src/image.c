/*$
 Copyright (c) 2021-2022 Azel

 This file is part of quantimg.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/********************************
 * イメージ処理
 ********************************/

#include <string.h>

#include "mlk.h"
#include "mlk_imagebuf.h"
#include "mlk_loadimage.h"
#include "mlk_saveimage.h"

#include "def.h"


/** 減色なしでパレットイメージへ */

void image_to_palimg(uint8_t **ppbuf,int width,int height)
{
	uint8_t *pd,*ps,*ppal;
	int ix,iy,i,palnum;

	palnum = g_app.dstpalnum;

	for(iy = height; iy; iy--)
	{
		pd = ps = *(ppbuf++);
		
		for(ix = width; ix; ix--, ps += 4)
		{
			//パレット検索

			ppal = g_app.palbuf;

			for(i = 0; i < palnum; i++, ppal += 4)
			{
				if(ps[0] == ppal[0] && ps[1] == ppal[1] && ps[2] == ppal[2])
					break;
			}
		
			*(pd++) = i;
		}

		progress_add(1, 0);
	}
}


//=========================
// 画像読み込み
//=========================


static mFuncLoadImageCheck g_load_checks[] = {
	mLoadImage_checkPNG, mLoadImage_checkJPEG, mLoadImage_checkBMP,
	mLoadImage_checkGIF, mLoadImage_checkWEBP, 0
};

/** 画像ファイル読み込み */

mImageBuf2 *image_load(const char *filename)
{
	mLoadImage li;
	mLoadImageType type;
	mImageBuf2 *img = NULL;
	const char *errmes = "error\n";
	mlkerr err;

	mLoadImage_init(&li);

	li.open.type = MLOADIMAGE_OPEN_FILENAME;
	li.open.filename = filename;
	li.convert_type = MLOADIMAGE_CONVERT_TYPE_RGBA;

	//ヘッダからタイプ判定

	err = mLoadImage_checkFormat(&type, &li.open, g_load_checks, 0);
	if(err)
	{
		if(err == MLKERR_UNSUPPORTED)
			putmes("unsupported format\n");
		else
			putmes(errmes);

		return NULL;
	}

	//開く

	if((type.open)(&li)) goto ERR;

	//最大サイズ

	if(li.width > IMAGESIZE_MAX || li.height > IMAGESIZE_MAX)
	{
		errmes = "image size is too large\n";
		goto ERR;
	}

	//画像確保

	img = mImageBuf2_new(li.width, li.height, 32, 0);
	if(!img) goto ERR;

	//イメージ読み込み

	li.imgbuf = img->ppbuf;

	if((type.getimage)(&li))
	{
		errmes = "decode error\n";
		goto ERR;
	}

	//透過色

	if(!li.trns.flag)
		g_app.tpcol = -1;
	else
		g_app.tpcol = (li.trns.r << 16) | (li.trns.g << 8) | li.trns.b;

	//

	(type.close)(&li);

	return img;

	//エラー
ERR:
	(type.close)(&li);
	mImageBuf2_free(img);
	putmes(errmes);
	return NULL;
}

/** 読み込み後の画像処理
 *
 * - 背景白とアルファ合成。
 * - 各色の 3byte 目に、透過色かどうかのフラグをセット。
 *
 * tpcol: -1 で透過色なし
 * return: 1 で、透過色の色が存在する */

int image_after_load(mImageBuf2 *img,int tpcol)
{
	uint8_t **pp,*pd;
	int ix,iy,a,havetp;

	pp = img->ppbuf;
	havetp = 0;

	for(iy = img->height; iy; iy--)
	{
		pd = *(pp++);
	
		for(ix = img->width; ix; ix--, pd += 4)
		{
			a = pd[3];
		
			if(a == 0)
			{
				pd[0] = pd[1] = pd[2] = 255;
			}
			else if(a != 255)
			{
				pd[0] = (pd[0] - 255) * a / 255 + 255;
				pd[1] = (pd[1] - 255) * a / 255 + 255;
				pd[2] = (pd[2] - 255) * a / 255 + 255;
			}

			//透過色フラグ

			if(tpcol != -1 && (tpcol == ((pd[0] << 16) | (pd[1] << 8) | pd[2])))
			{
				pd[3] = 1;
				havetp = 1;
			}
			else
				pd[3] = 0;
		}
	}

	return havetp;
}


//=========================
// 画像保存
//=========================


static mFuncSaveImage g_save_funcs[] = {
	mSaveImagePNG, mSaveImageBMP, mSaveImageGIF, mSaveImageWEBP
};


/* Y1行出力 */

static mlkerr _save_setrow(mSaveImage *p,int y,uint8_t *buf,int line_bytes)
{
	uint8_t **ppbuf = (uint8_t **)p->param1;

	memcpy(buf, ppbuf[y], line_bytes);

	return MLKERR_OK;
}

/* 進捗 */

static void _save_progress(mSaveImage *p,int percent)
{
	//percent 100 = height とする。
	//prog_offset からの相対値

	progress_add(-1, p->height * percent / 100);
}

/* WebP の場合、RGBA に変換 */

static void _pal_to_rgba(mImageBuf2 *img)
{
	uint8_t **ppbuf,*pd,*ps,*ppal;
	int ix,iy,src_rpos,dst_rpos,i,tpi;

	ppbuf = img->ppbuf;
	dst_rpos = (img->width - 1) * 4;
	src_rpos = img->width - 1;

	tpi = (g_app.is_img_have_tp)? g_app.dstpalnum - 1: -1;

	for(iy = img->height; iy; iy--)
	{
		pd = *(ppbuf++);
		ps = pd + src_rpos;
		pd = pd + dst_rpos;
	
		for(ix = img->width; ix; ix--, ps--, pd -= 4)
		{
			i = *ps;
			ppal = g_app.palbuf + i * 4;
			
			pd[0] = ppal[0];
			pd[1] = ppal[1];
			pd[2] = ppal[2];

			if(i == tpi)
				pd[3] = 0;
			else
				pd[3] = 255;
		}
	}
}

/** 画像の出力 */

mlkerr image_savefile(mImageBuf2 *img,const char *filename)
{
	mSaveImage si;
	mSaveImageOpt opt;
	int format;

	format = g_app.out_format;

	//mSaveImage

	mSaveImage_init(&si);

	si.open.type = MSAVEIMAGE_OPEN_FILENAME;
	si.open.filename = filename;
	si.coltype = MSAVEIMAGE_COLTYPE_PALETTE;
	si.width = img->width;
	si.height = img->height;
	si.bits_per_sample = 8;
	si.samples_per_pixel = 1;
	si.palette_buf = g_app.palbuf;
	si.palette_num = g_app.dstpalnum;
	si.setrow = _save_setrow;
	si.progress = _save_progress;
	si.param1 = img->ppbuf;

	//設定

	opt.mask = 0;

	switch(format)
	{
		//PNG
		case OUTFORMAT_PNG:
			opt.png.mask = MSAVEOPT_PNG_MASK_COMP_LEVEL;
			opt.png.comp_level = g_app.fopt.png.level;

			if(g_app.is_img_have_tp)
			{
				opt.png.mask |= MSAVEOPT_PNG_MASK_TRANSPARENT;
				opt.png.transparent = g_app.dstpalnum - 1;
			}
			break;

		//GIF
		case OUTFORMAT_GIF:
			if(g_app.is_img_have_tp)
			{
				opt.gif.mask = MSAVEOPT_GIF_MASK_TRANSPARENT;
				opt.gif.transparent = g_app.dstpalnum - 1;
			}
			break;

		//WebP
		case OUTFORMAT_WEBP:
			opt.webp.mask = MSAVEOPT_WEBP_MASK_LOSSY
				| MSAVEOPT_WEBP_MASK_LEVEL
				| MSAVEOPT_WEBP_MASK_QUALITY;

			opt.webp.lossy = (g_app.fopt.webp.quality != -1);
			opt.webp.level = g_app.fopt.webp.level;
			opt.webp.quality = g_app.fopt.webp.quality;

			//パレット非対応のため、RGBA に変換
			
			si.coltype = MSAVEIMAGE_COLTYPE_RGB;
			si.samples_per_pixel = 4;

			_pal_to_rgba(img);
			break;
	}

	//保存

	g_app.prog_offset = g_app.prog_curcnt;

	return (g_save_funcs[format - 1])(&si, &opt);
}

