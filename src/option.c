/*$
 Copyright (c) 2021-2022 Azel

 This file is part of quantimg.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

/********************************
 * オプション処理
 ********************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "mlk.h"
#include "mlk_str.h"
#include "mlk_string.h"
#include "mlk_file.h"
#include "mlk_argparse.h"

#include "def.h"


//--------------

#define VERSION_TEXT  "quantimg ver 1.0.1\nCopyright (c) 2022 Azel\n\nxPadie / Copyright (C) 1997-1999 Tamagawa Gengorou"

static const char *g_dither_name[] = {
	"none", "error", "mean", "tone"
};

typedef struct
{
	mArgParse ap;
	char *fopt_str;
}_argparse;

//--------------


/* 引数から数値を取得
 *
 * 範囲外の値の場合、エラー終了 */

static int _getopt_val(char *arg,int min,int max,const char *optname)
{
	int n;

	n = atoi(arg);

	if(n >= min && n <= max)
		return n;
	else
	{
		puterr_exit("--%s: invalid value", optname);
		return -1;
	}
}

/* キーワードとなる文字列からインデックス取得
 *
 * name : 検索する名前
 * list : 列挙文字列 (';' で区切る)
 * optname : 長い名前のオプション名 (エラー表示時用) */

static int _getopt_keyword(char *name,const char *list,const char *optname)
{
	int ind;

	ind = mStringGetSplitTextIndex(name, -1, list, ';', FALSE);
	if(ind != -1) return ind;

	puterr_exit("--%s: '%s' is invalid", optname, name);

	return -1;
}


//==============================
// フォーマットオプション処理
//==============================

/* t: 0 で値の初期化
 * return: 0 以外でエラー */
 
typedef int (*formatopt_func)(int t,const char *val,int len);


/* 数値取得 */

static int _fopt_getval(const char *val,int min,int max)
{
	int n;

	n = atoi(val);

	if(n < min || n > max)
	{
		puterr_exit("--fopt: invalid value");
		return 0;
	}

	return n;
}

/* PNG */

static int _fopt_png(int t,const char *val,int len)
{
	switch(t)
	{
		case 0:
			g_app.fopt.png.level = 6;
			break;
		case -1:
			putmes("# PNG: level=%d\n", g_app.fopt.png.level);
			break;
		case 'l':
			g_app.fopt.png.level = _fopt_getval(val, 0, 9);
			break;
		default:
			return 1;
	}

	return 0;
}

/* WebP */

static int _fopt_webp(int t,const char *val,int len)
{
	switch(t)
	{
		case 0:
			g_app.fopt.webp.level = 6;
			g_app.fopt.webp.quality = -1;
			break;
		case -1:
			if(g_app.fopt.webp.quality == -1)
				putmes("# WebP: lossless, level=%d\n", g_app.fopt.webp.level);
			else
				putmes("# WebP: lossy, quality=%d\n", g_app.fopt.webp.quality);
			break;
		case 'l':
			g_app.fopt.webp.level = _fopt_getval(val, 0, 9);
			g_app.fopt.webp.quality = -1;
			break;
		case 'q':
			g_app.fopt.webp.quality = _fopt_getval(val, 0, 100);
			break;
		default:
			return 1;
	}

	return 0;
}

/* BMP */

static int _fopt_bmp(int t,const char *val,int len)
{
	if(t == -1)
		putmes("# BMP\n");

	return 0;
}

/* GIF */

static int _fopt_gif(int t,const char *val,int len)
{
	if(t == -1)
		putmes("# GIF\n");

	return 0;
}

static formatopt_func g_fopt_funcs[] = {
	_fopt_png, _fopt_bmp, _fopt_gif, _fopt_webp
};

/* フォーマットオプションセット,情報表示
 *
 * arg: NULL で指定なし */

static void _set_format_opt(char *arg)
{
	formatopt_func func;
	const char *pc,*end;
	int len;

	func = g_fopt_funcs[g_app.out_format - 1];

	//値の初期化

	(func)(0, 0, 0);

	//文字列から各値を処理

	if(arg)
	{
		pc = arg;

		while(mStringGetNextSplit(&pc, &end, ','))
		{
			len = end - pc;

			if(len >= 3 && pc[1] == '=')
			{
				if((func)(*pc, pc + 2, len - 2))
					puterr_exit("--fopt: unknown option: '%c'", *pc);
			}
			else
				puterr_exit("--fopt: invalid value: \"%*s\"", len, pc);

			pc = end;
		}
	}

	//情報表示

	(func)(-1, 0, 0);
}


//==========================
// オプション処理
//==========================


/* --output: 出力パス */

static void _opt_output(mArgParse *p,char *arg)
{
	mStrSetText_locale(&g_app.str_output, arg, -1);

	//パスが、存在するディレクトリかどうか

	if(mStrIsnotEmpty(&g_app.str_output)
		&& mIsExistDir(g_app.str_output.buf))
	{
		g_app.is_output_dir = TRUE;
	}
}

/* --type: 出力形式
 *
 * デフォルトで -o による自動判別 */

static void _opt_type(mArgParse *p,char *arg)
{
	g_app.out_format = _getopt_keyword(arg,
		"png;bmp;gif;webp", "type") + 1;
}

/* --fopt: フォーマットオプション */

static void _opt_fopt(mArgParse *p,char *arg)
{
	((_argparse *)p)->fopt_str = arg;
}

/* --num: 色数 */

static void _opt_num(mArgParse *p,char *arg)
{
	g_app.colnum = _getopt_val(arg, 2, 256, "num");
}

/* --dither: ディザタイプ */

static void _opt_dither(mArgParse *p,char *arg)
{
	if(mStringIsNum(arg))
		g_app.dither_type = _getopt_val(arg, 0, 3, "dither");
	else
	{
		g_app.dither_type = _getopt_keyword(arg,
			"none;err;mean;tone", "dither");
	}
}

/* --dlevel: ディザ強さ */

static void _opt_dlevel(mArgParse *p,char *arg)
{
	g_app.dither_level = _getopt_val(arg, 1, 10, "dlevel");
}

/* --tp: 透過色指定 (r,g,b) */

static void _opt_tpcol(mArgParse *p,char *arg)
{
	int val[3],i,n;

	val[0] = val[1] = val[2] = 0;
	
	mStringGetSplitInt(arg, ',', val, 3);

	for(i = 0; i < 3; i++)
	{
		n = val[i];
		if(n < 0) n = 0;
		else if(n > 255) n = 255;

		val[i] = n;
	}

	g_app.opt_tpcol = MLK_RGB(val[0], val[1], val[2]);
}

/* --quiet */

static void _opt_quiet(mArgParse *p,char *arg)
{
	g_app.fputmes = 0;
}

/* --version */

static void _opt_version(mArgParse *p,char *arg)
{
	puts(VERSION_TEXT);
	exit_app(0);
}

/* ヘルプ表示 */

static void _put_help(const char *exename)
{
	printf("Usage: %s [option] <files...>\n\n", exename);

	puts(
"supported format: BMP/PNG/GIF/WebP/JPEG(input)\n"
"default output: <CURRENT_DIR>/<NAME>_q.<EXT>\n"
"\n"
"Options:\n"
"  -o,--output [path]  output filename or directory\n"
"  -t,--type [type]    output format (default=png)\n"
"       bmp/png/gif/webp\n"
"       when a file is specified with -o,\n"
"       it is automatically determined from the extension.\n"
"  -f,--fopt [opt]     options for output format\n"
"  -n,--num [num]      number of colors after color reduction\n"
"       (2-256, default=256) including transparent color\n"
"  -d,--dither [type]  dither type (default=err)\n"
"       0 or none: no dither\n"
"       1 or err: error diffusion\n"
"       2 or mean: mean error\n"
"       3 or tone: halftone dot\n"
"  -l,--dlevel [num]   dither strength (1-10, default=2)\n"
"  -p,--tp [R,G,B]     transparent color (R,G,B = 0-255)\n"
"       if transparent color is specified in the input image,\n"
"       give priority to it.\n"
"  -q,--quiet          quiet mode\n"
"  -v,--version        print program version\n"
"  -h,--help           display this help\n"
"\n"
"Format options (-f,--fopt):\n"
"  PNG  'l=[0-9]' (default: l=6)\n"
"         l = compression level\n"
"  WebP 'l=[0-9] or q=[0-100]' (default: l=6)\n"
"         l = lossless format, compression level\n"
"         q = lossy format, quality\n"
);

	exit_app(0);
}

/* --help */

static void _opt_help(mArgParse *p,char *arg)
{
	_put_help(p->argv[0]);
}

/* 出力ファイル名の拡張子から、フォーマットを自動判別 */

static void _set_outformat(void)
{
	mStr ext = MSTR_INIT;
	char *name;
	int n;

	mStrPathGetExt(&ext, g_app.str_output.buf);
	name = ext.buf;

	if(strcasecmp(name, "png") == 0)
		n = OUTFORMAT_PNG;
	else if(strcasecmp(name, "webp") == 0)
		n = OUTFORMAT_WEBP;
	else if(strcasecmp(name, "bmp") == 0)
		n = OUTFORMAT_BMP;
	else if(strcasecmp(name, "gif") == 0)
		n = OUTFORMAT_GIF;
	else
		n = OUTFORMAT_PNG;

	g_app.out_format = n;

	mStrFree(&ext);
}

/** オプション取得
 *
 * return: 通常引数の位置 */

int app_get_options(int argc,char **argv)
{
	mArgParseOpt opts[] = {
		{"output",'o',1,_opt_output},
		{"type",'t',1,_opt_type},
		{"fopt",'f',1,_opt_fopt},
		{"num",'n',1,_opt_num},
		{"dither",'d',1,_opt_dither},
		{"dlevel",'l',1,_opt_dlevel},
		{"tp",'p',1,_opt_tpcol},
		{"quiet",'q',0,_opt_quiet},
		{"version",'v',0,_opt_version},
		{"help",'h',0,_opt_help},
		{0,0,0,0}
	};
	_argparse dat;
	int ind;

	dat.ap.argc = argc;
	dat.ap.argv = argv;
	dat.ap.opts = opts;
	dat.fopt_str = NULL;

	ind = mArgParseRun(&dat.ap);

	//解析エラー

	if(ind == -1)
		exit_app(1);

	//入力ファイルがない

	if(ind == argc)
		_put_help(argv[0]);

	//出力フォーマット自動判別

	if(g_app.out_format == OUTFORMAT_AUTO)
	{
		if(mStrIsEmpty(&g_app.str_output) || g_app.is_output_dir)
			//-o 指定がない or ディレクトリの場合、PNG
			g_app.out_format = OUTFORMAT_PNG;
		else
			_set_outformat();
	}

	//設定の表示

	putmes("# colnum=%d, dither=%s, dither_level=%d\n",
		g_app.colnum, g_dither_name[g_app.dither_type], g_app.dither_level);

	//フォーマットオプション値
	//(出力形式の決定後)

	_set_format_opt(dat.fopt_str);

	return ind;
}

