/*$
 Copyright (c) 2021-2022 Azel

 This file is part of quantimg.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

//========================
// 減色作業用
//========================

typedef struct _ColorSpace ColorSpace;
typedef struct _quantwork quantwork;


//作業用の色空間

typedef struct
{
	int32_t x,y,z;
}GCS;

//パレットの色

typedef struct
{
	int32_t col; //RGB
	GCS gcs;
}PALDAT;

//色とカウント

typedef struct
{
	double x,y,z,c;
}RMA;

//ソース画像の使用色のデータ

typedef struct
{
	GCS gcs;	//色
	int cnt;	//色の参照数
	double flag;
}MED;

//メディアンカット用区画、リストアイテム

typedef struct
{
	mListItem i;

	RMA prin,std,avg;
	GCS mid;		//平均色
	double strain,	//区間内の色と平均色との距離の総和
		firstd;
	int bufstart,	//ppmedbuf 配列内の開始位置
		bufnum,		//ppmedbuf の start からの数
		rgb;		//平均色の RGB
}MedItem;

/* 減色作業用 */

typedef struct _Quant
{
	ColorSpace *cs;
	
	uint8_t **ppimg;	//ソース画像 (32bit)
	int imgw,imgh,
		full_used_colnum,	//透過色含む、ソース画像で使用されている色数
		used_colnum,	//ソース画像で使用されている色数 (透過色は除く)
		makepalnum,		//減色で作成する色の数 (透過色は除く)
		lastpalnum,		//最終的な出力色数 (透過色含む。減色が必要ない場合は、画像内の色数)
		frun_quant;		//減色を行ったか (FALSE で元の色のまま)

	int ddx,ddy,ddz;	//ディザ用

	MED *medbuf,		//使用色分の配列
		**ppmedbuf;		//medbuf のポインタの配列
	PALDAT *paldat;		//パレット (256色分)
	quantwork *work;	//各処理の作業用

	mList list_med;		//メディアンカット区画リスト
}Quant;

//------------

//colspace.c

double colorspace_distanceGCS(GCS *g1,GCS *g2);
double colorspace_distanceGCSd(GCS *g1,int x2,int y2,int z2);
void colorspace_convRGBtoGCS(ColorSpace *p,GCS *dst,int rgb);
int colorspace_convGCStoRGB(ColorSpace *p,double x,double y,double z);

//mediancut.c

mlkerr mediancut_make(Quant *p,MedItem *pi,int start,int num);
mlkerr mediancut_divide(Quant *p,MedItem *pisrc);

//convert.c

mlkerr quant_convert_image(Quant *p);
