/*$
 Copyright (c) 2021-2022 Azel

 This file is part of quantimg.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/
/*
 * 減色処理は、多摩川源五郎氏の xPadie のソースを元にしています。
 * Copyright (C) 1997-1999 Tamagawa Gengorou
 */

/**********************************
 * パレット画像への変換
 **********************************/

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "mlk.h"

#include "def.h"
#include "quant.h"


//----------------

#define CUBEX	(40)
#define CUBEY	(40)
#define CUBEZ	(40)
#define CUBEZO	(64*512*16)
#define CUBEALL	(CUBEX*CUBEY*CUBEZ)
#define CUBEH	(64*2560*16)
#define CUBESTEP	(64*128*16)
#define CUBESTEPH	(64*64*16)

//----------------

struct _quantwork
{
	int16_t *qcube[CUBEALL],
		indbuf[256];
	double dt[256];
};

//----------------


/* cube のインデックス位置を取得
 *
 * return: -1 で範囲外 */

static int _get_cubeadr(int x,int y,int z)
{
	int cx,cy,cz;

    cx = (x + CUBEH) / CUBESTEP;
    cy = (y + CUBEH) / CUBESTEP;
    cz = (z + CUBEZO) / CUBESTEP;

    if(cx < 0 || cx >= CUBEX || cy < 0 || cy >= CUBEY || cz < 0 || cz >= CUBEZ)
		return -1;
    
    return (cz * CUBEY + cy) * CUBEX + cx;
}

/* cube 作成 */

static void _make_cube(Quant *p,int adr,int x,int y,int z)
{
	int cnt,cx,cy,cz,i,num;
	double d,min,*pdt;
	GCS gcs;
	PALDAT *ppal;
	int16_t *pind;

	cx = (x + CUBEH) / CUBESTEP;
	cy = (y + CUBEH) / CUBESTEP;
	cz = (z + CUBEZO) / CUBESTEP;

	gcs.x = CUBESTEP * cx + CUBESTEPH - CUBEH;
	gcs.y = CUBESTEP * cy + CUBESTEPH - CUBEH;
	gcs.z = CUBESTEP * cz + CUBESTEPH - CUBEZO;

	num = p->makepalnum;

	//dt に各パレット色との距離をセット & 最小値取得

	pdt = p->work->dt;
	ppal = p->paldat;
	min = 0;

	for(i = 0; i < num; i++, ppal++)
	{
		d = sqrt(colorspace_distanceGCS(&gcs, &ppal->gcs));

		*(pdt++) = d;
	
		if(i == 0 || d < min)
			min = d;
	}

	//(最小距離 + 指定長さ)未満のパレットのインデックスを列挙

	pdt = p->work->dt;
	pind = p->work->indbuf;
	cnt = 0;

	//len = sqrt((double)CUBESTEP * CUBESTEP * 3);
	min += 227023.36345;
	
	for(i = 0; i < num; i++, pdt++)
	{
		if(*pdt < min)
		{
			*(pind++) = i;
			cnt++;
		}
	}

	//確保

	pind = (int16_t *)mMalloc((cnt + 1) * 2);

	p->work->qcube[adr] = pind;

	memcpy(pind, p->work->indbuf, cnt * 2);
	
	pind[cnt] = -1;
}

/* パレットインデックスのバッファから検索 */

static int _find_palette_buf(Quant *p,int16_t *buf,int x,int y,int z)
{
    GCS gcs;
	double min,t;
    int ind,ret = 0,f = 1;

	gcs.x = x;
	gcs.y = y;
	gcs.z = z;

	while(1)
	{
		ind = *(buf++);
		if(ind < 0) break;
	
		t = colorspace_distanceGCS(&gcs, &p->paldat[ind].gcs);

		if(f || t < min)
		{
			min = t;
			ret = ind;
			f = 0;
		}
	}

	return ret;
}

/* 色からパレット取得 */

static int _find_palette(Quant *p,int x,int y,int z)
{
	int pal,j,i,num;
	double min,t;
	GCS gcs;
	PALDAT *ppal;

	i = _get_cubeadr(x, y, z);
	
	if(i >= 0)
	{
		//qcube から取得
		
		if(!p->work->qcube[i])
			_make_cube(p, i, x, y, z);

		pal = _find_palette_buf(p, p->work->qcube[i], x, y, z);
	}
	else
	{
		//パレットの中で一番近い色
		
		gcs.x = x;
		gcs.y = y;
		gcs.z = z;

		ppal = p->paldat;
		num = p->makepalnum;

		min = colorspace_distanceGCS(&gcs, &ppal->gcs);
		pal = 0;
		ppal++;

		for(j = 1; j < num; j++, ppal++)
		{
			t = colorspace_distanceGCS(&gcs, &ppal->gcs);
			
			if(t < min)
			{
				min = t;
				pal = j;
			}
		}
	}
	
	return pal;
}


//============================
// ディザなし
//============================


static mlkerr _dither_none(Quant *p)
{
	uint8_t **ppbuf,*pd,*ps,tpind;
	int ix,iy;
	GCS gcs;

	ppbuf = p->ppimg;
	tpind = g_app.dstpalnum - 1;

	for(iy = p->imgh; iy; iy--)
	{
		pd = ps = *(ppbuf++);
	
		for(ix = p->imgw; ix; ix--)
		{
			if(ps[3])
				*pd = tpind;
			else
			{
				colorspace_convRGBtoGCS(p->cs, &gcs, (ps[0] << 16) | (ps[1] << 8) | ps[2]);

				*pd = _find_palette(p, gcs.x, gcs.y, gcs.z);
			}
		
			ps += 4;
			pd++;
		}

		progress_add(1, 0);
	}

	return MLKERR_OK;
}


//============================
// 誤差拡散/平均誤差
//============================


static const uint8_t g_fb_pat[2][3] = {
	{ 7,0,0 },
	{ 3,5,1 }
};

static const uint8_t g_mean_pat[3][5] = {
    { 5,7,0,0,0 },
    { 3,5,7,5,3 },
	{ 1,3,5,3,1 }
};

static void _free_dither_err(GCS **buf)
{
	int i;

	for(i = 0; i < 3; i++)
		mFree(buf[i]);
}

static mlkerr _dither_err(Quant *p,int type)
{
	uint8_t **ppbuf,*ps,*pd,tpind;
	int ix,iy,jx,jy,dir,dx,dy,dz,m,xpos,bufsize,level,src_right;
	int ddx,ddy,ddz;
	int64_t dxm,dym,dzm;
	GCS gcs,*buf[3],*pgcs,*pgcs2;

	//3行分の GCS バッファ

	buf[0] = buf[1] = buf[2] = NULL;
	
	bufsize = sizeof(GCS) * (p->imgw + 4);

	for(ix = 0; ix < 3; ix++)
	{
		buf[ix] = (GCS *)mMalloc0(bufsize);
		if(!buf[ix])
		{
			_free_dither_err(buf);
			return MLKERR_ALLOC;
		}
	}

	//

	ddx = p->ddx;
	ddy = p->ddy;
	ddz = p->ddz;
	level = g_app.dither_level;
	tpind = g_app.dstpalnum - 1;

	ppbuf = p->ppimg;
	dir = 1;
	src_right = (p->imgw - 1) * 4;

	//

	for(iy = p->imgh; iy; iy--)
	{
		ps = pd = *ppbuf;
		xpos = 2; //buf の x 位置

		//左方向の場合は、右端の位置に出力
		//(未処理の位置に上書きしないように)
		
		if(dir < 0)
		{
			ps += src_right;
			pd = ps + 3;
			xpos += p->imgw - 1;
		}

		//
	
		for(ix = p->imgw; ix; ix--, xpos += dir)
		{
			if(ps[3])
				*pd = tpind;
			else
			{
				colorspace_convRGBtoGCS(p->cs, &gcs, (ps[0] << 16) | (ps[1] << 8) | ps[2]);

				//dx,dy,dz

				dxm = dym = dzm = 0;

				if(type == 0)
				{
					//誤差拡散

					for(jx = -1; jx <= 1; jx++)
					{
						for(jy = 0; jy < 2; jy++)
						{
							m = g_fb_pat[jy][jx + 1];

							if(m > 0)
							{
								pgcs = buf[jy] + xpos + jx * dir;
								dxm += (int64_t)pgcs->x * m;
								dym += (int64_t)pgcs->y * m;
								dzm += (int64_t)pgcs->z * m;
							}
						}
					}

					//ディザ適用と、掛けた値の合計=16を割る

					dx = dxm * level / 160;
					dy = dym * level / 160;
					dz = dzm * level / 160;
				}
				else
				{
					//平均誤差

					for(jx = -2; jx <= 2; jx++)
					{
						for(jy = 0; jy < 3; jy++)
						{
							m = g_mean_pat[jy][jx + 2];

							if(m > 0)
							{
								pgcs = buf[jy] + xpos + jx * dir;
								dxm += (int64_t)pgcs->x * m;
								dym += (int64_t)pgcs->y * m;
								dzm += (int64_t)pgcs->z * m;
							}
						}
					}

					dx = dxm * level / 480;
					dy = dym * level / 480;
					dz = dzm * level / 480;
				}

				if(dx > ddx) dx = ddx; else if(dx < -ddx) dx = -ddx;
				if(dy > ddy) dy = ddy; else if(dy < -ddy) dy = -ddy;
				if(dz > ddz) dz = ddz; else if(dz < -ddz) dz = -ddz;

				//誤差を加算した色

				dx += gcs.x;
				dy += gcs.y;
				dz += gcs.z;

				m = _find_palette(p, dx, dy, dz);

				//パレット色との誤差をセット

				pgcs = buf[0] + xpos;
				pgcs2 = &p->paldat[m].gcs;
				
				pgcs->x = dx - pgcs2->x;
				pgcs->y = dy - pgcs2->y;
				pgcs->z = dz - pgcs2->z;

				//

				*pd = m;
			}
			
			if(dir > 0)
				ps += 4, pd++;
			else
				ps -= 4, pd--;
		}

		//左方向の場合、結果を左端に移動

		if(dir < 0)
			memmove(*ppbuf, pd + 1, p->imgw);

		//誤差のバッファをずらす

		pgcs = buf[2];
		buf[2] = buf[1];
		buf[1] = buf[0];
		buf[0] = pgcs;

		memset(buf[0], 0, bufsize);

		//

		ppbuf++;
		dir *= -1;

		progress_add(1, 0);
	}

	_free_dither_err(buf);

	return MLKERR_OK;
}


//=========================
// 網点
//=========================


typedef struct
{
	int pal,d;
}toneline;

static const uint8_t g_dtone[16] = { 0,8,2,10, 12,4,14,6, 3,11,1,9, 15,7,13,5 };

static int _qsort_tone(const void *a,const void *b)
{
	int n;

	n = ((toneline *)a)->d - ((toneline *)b)->d;

	if(n > 0)
		return 1;
	else if(n < 0)
		return -1;
	else
		return ((toneline *)a)->pal - ((toneline *)b)->pal;
}

static mlkerr _dither_tone(Quant *p)
{
	uint8_t **ppbuf,*pd,*ps,tpind;
	int ix,iy,j,dx,dy,dz,gx,gy,gz,k,level;
	toneline tlbuf[16],*ptl;
	GCS gcs,*pgcs;

	ppbuf = p->ppimg;
	level = g_app.dither_level;
	tpind = g_app.dstpalnum - 1;

	for(iy = 0; iy < p->imgh; iy++)
	{
		ps = pd = *(ppbuf++);
	
		for(ix = 0; ix < p->imgw; ix++)
		{
			if(ps[3])
				*pd = tpind;
			else
			{
				colorspace_convRGBtoGCS(p->cs, &gcs, (ps[0] << 16) | (ps[1] << 8) | ps[2]);

				//

				ptl = tlbuf;
				dx = dy = dz = 0;

				for(j = 0; j < 16; j++, ptl++)
				{
					gx = gcs.x + dx;
					gy = gcs.y + dy;
					gz = gcs.z + dz;

					k = _find_palette(p, gx, gy, gz);

					pgcs = &p->paldat[k].gcs;

					ptl->pal = k;
					ptl->d = pgcs->z;

					dx = (gx - pgcs->x) * level / 10;
					dy = (gy - pgcs->y) * level / 10;
					dz = (gz - pgcs->z) * level / 10;
				}

				//

				qsort(tlbuf, 16, sizeof(toneline), _qsort_tone);

				k = g_dtone[(iy & 3) * 4 + (ix & 3)];

				*pd = tlbuf[k].pal;
			}

			pd++;
			ps += 4;
		}

		progress_add(1, 0);
	}

	return MLKERR_OK;
}


//=========================
// main
//=========================


/* ppcube 解放 */

static void _free_qcube(int16_t **pp)
{
	int i;

	for(i = 0; i < CUBEALL; i++, pp++)
	{
		if(*pp) mFree(*pp);
	}
}

/** ソース画像をパレット画像に変換 */

mlkerr quant_convert_image(Quant *p)
{
	quantwork *work;
	mlkerr ret;

	//quantwork

	work = (quantwork *)mMalloc(sizeof(quantwork));
	if(!work) return MLKERR_ALLOC;

	mMemset0(work->qcube, CUBEALL * sizeof(void*));

	p->work = work;

	//

	switch(g_app.dither_type)
	{
		//誤差拡散
		case DITHER_ERR:
			ret = _dither_err(p, 0);
			break;
		//平均誤差
		case DITHER_MEAN:
			ret = _dither_err(p, 1);
			break;
		//網点
		case DITHER_TONE:
			ret = _dither_tone(p);
			break;
		//なし
		default:
			ret = _dither_none(p);
			break;
	}

	//解放

	_free_qcube(work->qcube);

	mFree(work);
	p->work = NULL;

	return ret;
}


