/*$
 Copyright (c) 2021-2022 Azel

 This file is part of quantimg.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/
/*
 * 減色処理は、多摩川源五郎氏の xPadie のソースを元にしています。
 * Copyright (C) 1997-1999 Tamagawa Gengorou
 */

/**********************************
 * メディアンカット処理
 **********************************/

#include <stdlib.h>
#include <math.h>

#include "mlk.h"
#include "mlk_list.h"

#include "quant.h"


//======================
// 区画の作成
//======================


static void _jacobi(int n,double a[][3],double u[][3],double eps)
{
	int m,i,j,k,loop;
	double p,q,t,s2,c2,c,s,r;

	loop = 1;

	for(m = 0; m < 10 && loop; m++)
	{
		loop = 0;

		for(i = 0; i < n - 1; i++)
		{
			for (j = i + 1; j < n; j++)
			{
				if(fabs(a[i][j]) < eps) continue;

				loop = 1;

				p = (a[i][i] - a[j][j]) / 2;
				q = a[i][j];
				t = p / q;
				s2 = 1 / sqrt(1 + t * t);
				if(q < 0) s2 = -s2;

				c2 = t * s2;

				if(c2 > 0)
				{
					c = sqrt((1 + c2) / 2);
					s = s2 / c / 2;
				}
				else
				{
					s = sqrt((1 - c2) / 2);
					c = s2 / s / 2;
				}

				r = a[i][i] + a[j][j];
				
				a[i][i] = r / 2 + p * c2 + q * s2;
				a[j][j] = r - a[i][i];
				a[i][j] = a[j][i] = 0;

				for(k = 0; k < n; k++)
				{
					if((k != i) && (k != j))
					{
						p = a[k][i];
						q = a[k][j];
						
						a[k][i] = a[i][k] =  p * c + q * s;
						a[k][j] = a[j][k] = -p * s + q * c;
					}
				}
				
				for(k = 0; k < n; k++)
				{
					p = u[k][i];
					q = u[k][j];
					
					u[k][i] =  p * c + q * s;
					u[k][j] = -p * s + q * c;
				}
			}
		}
	}
}

static double _set_rma(MED **medbuf,int start,int num,RMA *prin,RMA *std,RMA *avg)
{
	MED **ppmed,*pmed;
	int i,j,dx,dy;
	double d,dv[3],sum;
	double jca[3][3],jcw[3][3];

	avg->x = 0;
	avg->y = 0;
	avg->z = 0;
	avg->c = 0;

	if(num <= 0) return 0;

	//平均色

	ppmed = medbuf + start;
	sum = 0;

	for(i = 0; i < num; i++)
	{
		pmed = *(ppmed++);
		
		d = pmed->cnt;
		sum += d;

		avg->x += pmed->gcs.x * d;
		avg->y += pmed->gcs.y * d;
		avg->z += pmed->gcs.z * d;
	}

	avg->x /= sum;
	avg->y /= sum;
	avg->z /= sum;

	avg->c = std->c = prin->c = sum;

	//--- 共分散行列

	mMemset0(jca, sizeof(jca));
	mMemset0(jcw, sizeof(jcw));

	for(dx = 0; dx < 3; dx++)
		jcw[dx][dx] = 1;

	//jca

	ppmed = medbuf + start;

	for(i = 0; i < num; i++)
	{
		pmed = *(ppmed++);
		
		d = pmed->cnt;

		dv[0] = pmed->gcs.x - avg->x;
		dv[1] = pmed->gcs.y - avg->y;
		dv[2] = pmed->gcs.z - avg->z;

		for(dx = 0; dx < 3; dx++)
		{
			for(dy = 0; dy < 3; dy++)
				jca[dx][dy] += dv[dx] * dv[dy] * d;
		}
	}

	d = avg->c;

	for(dx = 0; dx < 3; dx++)
	{
		for(dy = 0; dy < 3; dy++)
			jca[dx][dy] /= d;
	}

	//std

	std->x = sqrt(jca[0][0]);
	std->y = sqrt(jca[1][1]);
	std->z = sqrt(jca[2][2]);

	//prin

	_jacobi(3, jca, jcw, pow(10, -16));

	j = 2;
	d = jca[2][2];

	for(i = 0; i < 2; i++)
	{
		if(d < jca[i][i])
		{
			j = i;
			d = jca[i][i];
		}
	}

	prin->x = jcw[0][j];
	prin->y = jcw[1][j];
	prin->z = jcw[2][j];

	return d;
}

/** 区画を作成 or セット
 *
 * pi: NULL で新規作成 */

mlkerr mediancut_make(Quant *p,MedItem *pi,int start,int num)
{
	MED **ppmed,*pmed;
	int i,mx,my,mz;
	double st;

	if(!pi)
	{
		pi = (MedItem *)mListAppendNew(&p->list_med, sizeof(MedItem));
		if(!pi) return MLKERR_ALLOC;
	}

	pi->bufstart = start;
	pi->bufnum = num;
	pi->firstd = _set_rma(p->ppmedbuf, start, num, &pi->prin, &pi->std, &pi->avg);

	//平均色

	mx = lround(pi->avg.x);
	my = lround(pi->avg.y);
	mz = lround(pi->avg.z);

	pi->mid.x = mx;
	pi->mid.y = my;
	pi->mid.z = mz;

	pi->rgb = colorspace_convGCStoRGB(p->cs, mx, my, mz);

	//平均色との距離の総和

	ppmed = p->ppmedbuf + start;
	st = 0;

	for(i = 0; i < num; i++)
	{
		pmed = *(ppmed++);
		
		st += colorspace_distanceGCSd(&pmed->gcs, mx, my, mz) * pmed->cnt;
	}

	pi->strain = st;

	return MLKERR_OK;
}


//======================
// 区間の分割
//======================


typedef struct
{
	double x,y,z;
}GCSd;

typedef struct
{
	GCSd total,sqr;
	double count;
}STC;


/* MED* のソート関数 */

static int _qsort_med(const void *a,const void *b)
{
	double d;

	d = (*((MED **)a))->flag - (*((MED **)b))->flag;

	if(d > 0)
		return 1;
	else if(d < 0)
		return -1;
	else
		return 0;
}

/* 分割位置取得 */

static int _find_div(MED **ppbuf,int st,int n)
{
	MED **ppmed,*pmed;
	GCSd a,b,d,e;
	STC left,right;
	int i,ret = 0;
	double mmin = 0,m;
	double x,y,z,c,xc,yc,zc,mc;

	mMemset0(&left, sizeof(STC));
	mMemset0(&right, sizeof(STC));

	//---- right 初期値

	ppmed = ppbuf + st;

	for(i = 0; i < n; i++)
	{
		pmed = *(ppmed++);
		
		x = pmed->gcs.x;
		y = pmed->gcs.y;
		z = pmed->gcs.z;
		c = pmed->cnt;

		xc = x * c;
		yc = y * c;
		zc = z * c;

		right.count += c;
		right.total.x += xc;
		right.total.y += yc;
		right.total.z += zc;

		right.sqr.x += xc * x;
		right.sqr.y += yc * y;
		right.sqr.z += zc * z;
	}

	a.x = right.total.x / right.count;
	a.y = right.total.y / right.count;
	a.z = right.total.z / right.count;

	right.sqr.x += a.x * a.x * right.count - 2 * a.x * right.total.x;
	right.sqr.y += a.y * a.y * right.count - 2 * a.y * right.total.y;
	right.sqr.z += a.z * a.z * right.count - 2 * a.z * right.total.z;

	//-----
	
	ppmed = ppbuf + st;
	
	for(i = 1; i < n; i++)
	{
		pmed = *(ppmed++);
	
		x = pmed->gcs.x;
		y = pmed->gcs.y;
		z = pmed->gcs.z;
		c = pmed->cnt;

		xc = x * c;
		yc = y * c;
		zc = z * c;

		//left

		mc = left.count;
		if(mc < 1.0) mc = 1.0;
		
		a.x = left.total.x / mc;
		a.y = left.total.y / mc;
		a.z = left.total.z / mc;

		mc = left.count + c;
		if(mc < 1.0) mc = 1.0;

		e.x = left.total.x + xc;
		e.y = left.total.y + yc;
		e.z = left.total.z + zc;

		b.x = e.x / mc;
		b.y = e.y / mc;
		b.z = e.z / mc;

		d.x = b.x - a.x;
		d.y = b.y - a.y;
		d.z = b.z - a.z;

		left.sqr.x += d.x * d.x * left.count;
		left.sqr.y += d.y * d.y * left.count;
		left.sqr.z += d.z * d.z * left.count;

		left.total.x = e.x;
		left.total.y = e.y;
		left.total.z = e.z;

		left.count = mc;

		left.sqr.x += (x - b.x) * (x - b.x) * c;
		left.sqr.y += (y - b.y) * (y - b.y) * c;
		left.sqr.z += (z - b.z) * (z - b.z) * c;

		//right

		mc = right.count;
		if(mc < 1.0) mc = 1.0;

		a.x = right.total.x / mc;
		a.y = right.total.y / mc;
		a.z = right.total.z / mc;

		mc = right.count - c;
		if(mc < 1.0) mc = 1.0;

		e.x = right.total.x - xc;
		e.y = right.total.y - yc;
		e.z = right.total.z - zc;

		b.x = e.x / mc;
		b.y = e.y / mc;
		b.z = e.z / mc;

		d.x = b.x - a.x;
		d.y = b.y - a.y;
		d.z = b.z - a.z;

		right.sqr.x += d.x * d.x * right.count;
		right.sqr.y += d.y * d.y * right.count;
		right.sqr.z += d.z * d.z * right.count;

		right.total.x = e.x;
		right.total.y = e.y;
		right.total.z = e.z;

		right.count = mc;

		right.sqr.x -= (x - b.x) * (x - b.x) * c;
		right.sqr.y -= (y - b.y) * (y - b.y) * c;
		right.sqr.z -= (z - b.z) * (z - b.z) * c;

		//最小値

		m = (left.sqr.x + left.sqr.y + left.sqr.z) + (right.sqr.x + right.sqr.y + right.sqr.z);

		if(i == 1 || m < mmin)
			mmin = m, ret = i;
	}

	return ret;
}

/** 区間の分割
 *
 * return: -1 で分割できなかった */

mlkerr mediancut_divide(Quant *p,MedItem *pisrc)
{
	MED **ppmed,*pmed;
	RMA prin;
	int start,num,i;

	start = pisrc->bufstart;
	num = pisrc->bufnum;

	if(num == 1) goto ERRONE;

	prin = pisrc->prin;

	//ソート用 flag

	ppmed = p->ppmedbuf + start;
	
	for(i = 0; i < num; i++)
	{
		pmed = *(ppmed++);
		
		pmed->flag = pmed->gcs.x * prin.x
			+ pmed->gcs.y * prin.y + pmed->gcs.z * prin.z;
	}

	//区間内の ppmedbuf をソート
	
	qsort(p->ppmedbuf + start, num, sizeof(void*), _qsort_med);

	//分割位置

	i = _find_div(p->ppmedbuf, start, num);

	if(num - i < 1)
	{
		if(num == 2)
			i = 1;
		else
			goto ERRONE;
	}

	//分割

	mediancut_make(p, pisrc, start, i);
	
	return mediancut_make(p, NULL, start + i, num - i);

	//分割失敗
ERRONE:
	mediancut_make(p, pisrc, start, num);
	pisrc->strain = 0;
	return -1;
}

