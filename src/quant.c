/*$
 Copyright (c) 2021-2022 Azel

 This file is part of quantimg.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/
/*
 * 減色処理は、多摩川源五郎氏の xPadie のソースを元にしています。
 * Copyright (C) 1997-1999 Tamagawa Gengorou
 */

/********************************
 * 減色処理
 ********************************/

#include <math.h>
#include <stdlib.h>

#include "mlk.h"
#include "mlk_imagebuf.h"
#include "mlk_list.h"

#include "def.h"
#include "quant.h"


//---------------

#define MLKERR_APP_NO_QUANT  -1	//減色の必要なし

//---------------


//===========================
// 輪郭と色の抽出処理
//===========================


/* 輪郭:値の取得 */

static uint8_t _sobel_getval(int v,int min,int max)
{
	int n;

	n = max - min;
	if(n < 8) n = 8;

	n = (int)(128.0 - (double)v / n * 64.0);

	if(n < 0) n = 0;
	else if(n > 255) n = 255;

	return n;
}

/* sobel の輪郭抽出
 *
 * - 端の 1px は処理なし。 */

static void _colproc_sobel(Quant *p,mImageBuf2 *tmp)
{
	uint8_t **ppsrc,**ppdst,*ps,*pd,*ps2;
	int ix,iy,jx,jy,r,g,b,pos,sval,xpos,imgw,imgh;
	int rx,ry,gx,gy,bx,by,rm,gm,bm;
	int rmax,rmin,gmax,gmin,bmax,bmin;
	int8_t sobel1[9] = {1,0,-1, 2,0,-2, 1,0,-1};
	int8_t sobel2[9] = {1,2,1, 0,0,0, -1,-2,-1};

	imgw = p->imgw;
	imgh = p->imgh;

	ppsrc = p->ppimg;
	ppdst = tmp->ppbuf;

	for(iy = 0; iy < imgh; iy++)
	{
		ps = *ppsrc;
		pd = *ppdst;
		xpos = -4; //x - 1 の src バッファ位置
	
		for(ix = 0; ix < imgw; ix++, ps += 4, pd += 6, xpos += 4)
		{
			if(ix == 0 || iy == 0 || ix == imgw - 1 || iy == imgh - 1)
			{
				pd[0] = pd[1] = pd[2] = pd[3] = pd[4] = pd[5] = 0;
			}
			else
			{
				rm = ps[0];
				gm = ps[1];
				bm = ps[2];
				
				rx = ry = gx = gy = bx = by = 0;
				rmax = gmax = bmax = 0;
				rmin = gmin = bmin = 1000;
				pos = 0;

				//3x3
				
				for(jy = -1; jy <= 1; jy++)
				{
					ps2 = ppsrc[jy] + xpos;
				
					for(jx = -1; jx <= 1; jx++, ps2 += 4, pos++)
					{
						r = ps2[0];
						g = ps2[1];
						b = ps2[2];

						//最小値と最大値

						if(r > rmax) rmax = r;
						if(g > gmax) gmax = g;
						if(b > bmax) bmax = b;

						if(r < rmin) rmin = r;
						if(g < gmin) gmin = g;
						if(b < bmin) bmin = b;

						//sobel

						r -= rm;
						g -= gm;
						b -= bm;

						sval = sobel1[pos];
						rx += sval * r;
						gx += sval * g;
						bx += sval * b;

						sval = sobel2[pos];
						ry += sval * r;
						gy += sval * g;
						by += sval * b;
					}
				}

				//値をセット

				pd[0] = _sobel_getval(rx, rmin, rmax);
				pd[1] = _sobel_getval(ry, rmin, rmax);
				pd[2] = _sobel_getval(gx, gmin, gmax);
				pd[3] = _sobel_getval(gy, gmin, gmax);
				pd[4] = _sobel_getval(bx, bmin, bmax);
				pd[5] = _sobel_getval(by, bmin, bmax);
			}
		}

		ppsrc++;
		ppdst++;

		progress_add(1, 0);
	}
}

/* 使用色が目的の色数以下の場合、使用色でパレット作成 */

static void _make_raw_palette(Quant *p,int32_t *cntbuf,int colcnt)
{
	PALDAT *ppal;
	int i,tpcol;

	//パレットセット (透過色除く)
	
	ppal = p->paldat;
	tpcol = g_app.tpcol;

	for(i = 0; i < 0x1000000; i++, cntbuf++)
	{
		if(*cntbuf && i != tpcol)
		{
			ppal->col = i;
			ppal++;
		}
	}

	//透過色は最後に追加

	if(g_app.is_img_have_tp)
		ppal->col = tpcol;

	//

	p->lastpalnum = colcnt;

	//減色パレット作成分

	progress_add(p->imgh, 0);
}

/* 使用色のデータ作成 (透過色は除く) */

static mlkerr _make_usedcol_data(Quant *p,int32_t *cntbuf)
{
	MED *pmed,**ppmed;
	int i,num,tpcol;

	num = p->used_colnum;

	//バッファ作成

	pmed = p->medbuf = (MED *)mMalloc(num * sizeof(MED));
	ppmed = p->ppmedbuf = (MED **)mMalloc(num * sizeof(void*));

	if(!pmed || !ppmed)
		return MLKERR_ALLOC;

	//色(GCS)と参照数セット

	tpcol = g_app.tpcol;

	for(i = 0; i < 0x1000000; i++, cntbuf++)
	{
		if(*cntbuf && i != tpcol)
		{
			colorspace_convRGBtoGCS(p->cs, &pmed->gcs, i);

			pmed->cnt = *cntbuf;

			*(ppmed++) = pmed;
			pmed++;
		}
	}

	return MLKERR_OK;
}

/* 使用色の抽出 */

static mlkerr _colproc_make(Quant *p,mImageBuf2 *imgtmp)
{
	uint8_t **ppsrc,**ppdst,*pd,*pd2,*ps;
	int32_t *cntbuf;
	int ix,iy,jx,jy,colcnt,imgw,imgh,xpos;
	int rx,ry,gx,gy,bx,by,fedge;
	int rxm,rym,gxm,gym,bxm,bym;
	double dr,dg,db;
	mlkerr ret;

	//全色のピクセル数カウントバッファ (64MB)

	cntbuf = (int32_t *)mMalloc0(0x1000000 * 4);
	if(!cntbuf) return MLKERR_ALLOC;

	//
	
	imgw = p->imgw;
	imgh = p->imgh;
	colcnt = 0;

	//---- 輪郭を判定し、各色の参照数をカウント

	ppsrc = p->ppimg;
	ppdst = imgtmp->ppbuf;

	for(iy = 0; iy < imgh; iy++)
	{
		ps = *ppsrc;
		pd = *ppdst;
		xpos = -6; //x - 1 の dst バッファ位置
	
		for(ix = 0; ix < imgw; ix++, ps += 4, pd += 6, xpos += 6)
		{
			//輪郭部分か

			if(ps[3] || ix == 0 || iy == 0 || ix == imgw - 1 || iy == imgh - 1)
				fedge = 0;
			else
			{
				rxm = pd[0];
				rym = pd[1];
				gxm = pd[2];
				gym = pd[3];
				bxm = pd[4];
				bym = pd[5];

				dr = dg = db = 0;

				for(jy = -1; jy <= 1; jy++)
				{
					pd2 = ppdst[jy] + xpos;
					
					for(jx = -1; jx <= 1; jx++, pd2 += 6)
					{
						rx = pd2[0] - rxm;
						ry = pd2[1] - rym;
						gx = pd2[2] - gxm;
						gy = pd2[3] - gym;
						bx = pd2[4] - bxm;
						by = pd2[5] - bym;

						dr += sqrt(rx * rx + ry * ry);
						dg += sqrt(gx * gx + gy * gy);
						db += sqrt(bx * bx + by * by);
					}
				}

				rx = (int)(255 - sqrt(dr * dr + dg * dg + db * db) / 8.0);
				
				fedge = (rx >= 160);
			}

			//RGB

			rx = (ps[0] << 16) | (ps[1] << 8) | ps[2];

			//色数

			if(!cntbuf[rx])
				colcnt++;

			//輪郭部分では、参照数を増やす

			cntbuf[rx] += (fedge)? 8: 1;
		}

		ppsrc++;
		ppdst++;

		progress_add(1, 0);
	}

	//---------

	//透過色含む使用色数

	p->full_used_colnum = colcnt;

	//

	if(colcnt <= g_app.colnum)
	{
		//---- 減色が必要ない場合

		//使用色でパレット作成 (透過色含む)

		_make_raw_palette(p, cntbuf, colcnt);

		ret = MLKERR_APP_NO_QUANT;
	}
	else
	{
		//---- 減色あり
		
		//透過色は除く、使用色数

		if(g_app.is_img_have_tp) colcnt--;

		p->used_colnum = colcnt;

		//使用色のデータ作成

		ret = _make_usedcol_data(p, cntbuf);
	}

	mFree(cntbuf);

	return ret;
}

/* ソース画像の色の処理 */

static mlkerr _proc_color(Quant *p)
{
	mImageBuf2 *tmp;
	mlkerr ret;

	tmp = mImageBuf2_new(p->imgw, p->imgh, 6 * 8, 0);
	if(!tmp) return MLKERR_ALLOC;

	//

	_colproc_sobel(p, tmp);

	ret = _colproc_make(p, tmp);

	//

	mImageBuf2_free(tmp);

	return ret;
}


//===========================
// 減色処理
//===========================


/* メディアンカット */

static mlkerr _mediancut(Quant *p)
{
	mList *list;
	PALDAT *ppal;
	MedItem *pi,*pi2;
	int k,palnum,cnt,col,flag;
	double max;
	mlkerr ret;

	list = &p->list_med;

	//全体を一区画に

	ret = mediancut_make(p, NULL, 0, p->used_colnum);
	if(ret) return ret;

	//最大色数まで分割 (透過色除く)

	g_app.prog_offset = g_app.prog_curcnt;

	palnum = 0;

	while(palnum < p->makepalnum && list->num < 760)
	{
		//平均色から一番離れている区間を取得

		flag = 1;

		MLK_LIST_FOR(*list, pi, MedItem)
		{
			if(flag || max < pi->strain)
			{
				pi2 = pi;
				max = pi->strain;
				flag = 0;
			}
		}

		if(max <= 0) break;

		//分割

		ret = mediancut_divide(p, pi2);

		if(ret < 0)
			continue;
		else if(ret)
			return ret;

		//パレット取得

		cnt = 0;

		MLK_LIST_FOR(*list, pi, MedItem)
		{
			//検索
			
			col = pi->rgb;
			ppal = p->paldat;
			flag = TRUE;

			for(k = 0; k < cnt; k++, ppal++)
			{
				if(ppal->col == col)
				{
					flag = FALSE;
					break;
				}
			}

			//追加

			if(flag)
			{
				ppal = p->paldat + cnt;

				ppal->col = col;
			
				colorspace_convRGBtoGCS(p->cs, &ppal->gcs, col);

				cnt++;
			}
		}

		palnum = cnt;

		//progress

		progress_add(-1, p->imgh * cnt / p->makepalnum);
	}

	//進捗:最大位置へ

	progress_add(-1, p->imgh);

	//解放

	mListDeleteAll(list);

	mFree(p->medbuf);
	mFree(p->ppmedbuf);
	p->medbuf = NULL;
	p->ppmedbuf = NULL;

	return MLKERR_OK;
}


//===========================
// パレット処理
//===========================


static const int g_wrapsign[13][3]= {
	{ 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 }, { 1, 1, 0 },
	{ 1, 0, 1 }, { 1, 0,-1 }, { 1,-1, 0 }, { 0, 1, 1 },
	{ 0, 1,-1 }, { 1, 1, 1 }, { 1,-1, 1 }, { 1,-1,-1 }, { 1, 1,-1 }
};

/* パレットのソート関数 */

static int _qsort_pal(const void *a,const void *b)
{
	return ((PALDAT *)a)->gcs.z - ((PALDAT *)b)->gcs.z;
}

/* パレット最終処理 */

static void _make_palette(Quant *p)
{
	PALDAT *ppal;
	int i,j,x,y,z,v;
	int wrapmx[13],wrapmn[13];

	//z 順にソート
	
	qsort(p->paldat, p->makepalnum, sizeof(PALDAT), _qsort_pal);

	//ディザ用、色差の最大値 (誤差拡散/平均誤差)

	if(g_app.dither_type == DITHER_ERR || g_app.dither_type == DITHER_MEAN)
	{
		ppal = p->paldat;

		for(i = 0; i < p->makepalnum; i++, ppal++)
		{
			x = ppal->gcs.x;
			y = ppal->gcs.y;
			z = ppal->gcs.z;

			for(j = 0; j < 13; j++)
			{
				v = x * g_wrapsign[j][0] + y * g_wrapsign[j][1] + z * g_wrapsign[j][2];

				if(i == 0)
					wrapmx[j] = wrapmn[j] = v;
				else
				{
					if(v < wrapmn[j]) wrapmn[j] = v;
					if(v > wrapmx[j]) wrapmx[j] = v;
				}
			}
		}

		p->ddx = wrapmx[0] - wrapmn[0];
		p->ddy = wrapmx[1] - wrapmn[1];
		p->ddz = wrapmx[2] - wrapmn[2];
	}

	//最終的な色数と、透過色追加

	p->lastpalnum = p->makepalnum;

	if(g_app.is_img_have_tp)
	{
		p->paldat[p->makepalnum].col = g_app.tpcol;
		p->lastpalnum++;
	}
}

/* 画像出力用パレットデータセット */

static void _set_dstpalette(Quant *p)
{
	PALDAT *ppal;
	uint8_t *pd;
	int i,c;

	ppal = p->paldat;
	pd = g_app.palbuf;

	for(i = p->lastpalnum; i > 0; i--, ppal++, pd += 4)
	{
		c = ppal->col;
		
		pd[0] = (uint8_t)(c >> 16);
		pd[1] = (uint8_t)(c >> 8);
		pd[2] = (uint8_t)c;
		pd[3] = 0;
	}

	g_app.dstpalnum = p->lastpalnum;
}


//===========================
// main
//===========================


/** Quant 終了時の解放 */

void quant_free(Quant *p)
{
	if(p)
	{
		mFree(p->paldat);

		mFree(p);
	}
}

/** Quant 作成 */

Quant *quant_create(void)
{
	Quant *p;

	p = (Quant *)mMalloc0(sizeof(Quant));
	if(!p) return NULL;

	//パレットデータ

	p->paldat = (PALDAT *)mMalloc(sizeof(PALDAT) * 256);
	if(!p->paldat)
	{
		mFree(p);
		return NULL;
	}

	return p;
}

/* 実行時の解放 */

static void _qfree(Quant *p)
{
	mFree(p->medbuf);
	mFree(p->ppmedbuf);
	p->medbuf = NULL;
	p->ppmedbuf = NULL;

	mListDeleteAll(&p->list_med);
}

/* 初期化 */

static void _qinit(Quant *p)
{
	PALDAT *ppal;
	int i;

	//パレット

	ppal = p->paldat;
	
	for(i = 0; i < 256; i++, ppal++)
	{
		ppal->col = 0;
		ppal->gcs.z = INT32_MAX;
	}
}

/** 減色処理 */

mlkerr quant_run(Quant *p,mImageBuf2 *img)
{
	mlkerr ret;

	p->cs = g_app.cs;
	p->ppimg = img->ppbuf;
	p->imgw = img->width;
	p->imgh = img->height;

	_qinit(p);

	//作成するパレット数 (透過色除く)

	p->makepalnum = g_app.colnum;

	if(g_app.is_img_have_tp) p->makepalnum--;

	//ソース画像、色の処理

	ret = _proc_color(p);
	
	if(ret && ret != MLKERR_APP_NO_QUANT) goto END;

	//パレットの作成とイメージ処理

	if(ret == MLKERR_APP_NO_QUANT)
	{
		//--- 減色処理なし

		_set_dstpalette(p);

		image_to_palimg(img->ppbuf, img->width, img->height);

		p->frun_quant = FALSE;

		ret = MLKERR_OK;
	}
	else
	{
		//--- 減色処理
		
		//メディアンカット

		ret = _mediancut(p);
		if(ret) goto END;

		//パレットの作成

		_make_palette(p);

		_set_dstpalette(p);

		//画像変換

		ret = quant_convert_image(p);

		p->frun_quant = TRUE;
	}

	//
END:
	_qfree(p);

	return ret;
}

