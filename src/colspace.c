/*$
 Copyright (c) 2021-2022 Azel

 This file is part of quantimg.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/
/*
 * 減色処理は、多摩川源五郎氏の xPadie のソースを元にしています。
 * Copyright (C) 1997-1999 Tamagawa Gengorou
 */

/**********************************
 * 作業用の色空間
 **********************************/

#include <math.h>

#include "mlk.h"

#include "quant.h"


//-------------------

//2x2行列

typedef struct
{
	double m11,m12,m21,m22;
}Matrix;

typedef struct _ColorSpace
{
	Matrix m1,m2;
	double gtable[256];
}ColorSpace;

//--------------------


/** 色空間の作成 */

ColorSpace *colorspace_create(void)
{
	ColorSpace *p;
	int i;
	double d,*pd;

	p = (ColorSpace *)mMalloc(sizeof(ColorSpace));
	if(!p) return NULL;

	//行列

	p->m1.m11 = 0x1.e24d5d56d6fd4p-1;
	p->m1.m12 = 0x1.1732a8382badbp-3;
	p->m1.m21 = 0x1.e4286350bf9ecp-4;
	p->m1.m22 = 0x1.3390a9deb3132p+0;
	
	p->m2.m11 = 0x1.13b047f35c646p+0;
	p->m2.m12 = -0x1.f485d6cd64888p-4;
	p->m2.m21 = -0x1.b1fa9c6dab0b2p-4;
	p->m2.m22 = 0x1.b050d1df68819p-1;

	//gtable

	pd = p->gtable;

	for(i = 0; i < 256; i++)
	{
		d = i / 255.0;

		if(d >= 0.117647058823529411764705882352941)
			d = pow(d, 3.0/2.2);
		else
			d = pow(d, 1.0/2.2) * 0.142913647595774056881018286010431;

		*(pd++) = d;
	}

	return p;
}

/** GCS 間の距離取得 */

double colorspace_distanceGCS(GCS *g1,GCS *g2)
{
	double x,y,z;

	z = (g1->z - g2->z);
	x = (g1->x - g2->x);
	y = (g1->y - g2->y);

	return (x * x) + (y * y) + (z * z);
}

/** GCS 間の距離取得 (2番目は直接指定) */

double colorspace_distanceGCSd(GCS *g1,int x2,int y2,int z2)
{
	double x,y,z;

	z = (g1->z - z2);
	x = (g1->x - x2);
	y = (g1->y - y2);

	return (x * x) + (y * y) + (z * z);
}

/** RGB -> GCS */

void colorspace_convRGBtoGCS(ColorSpace *p,GCS *dst,int rgb)
{
	double r,g,b,y,cb,cr,xx,yy;

	r = p->gtable[(rgb >> 16) & 255];
	g = p->gtable[(rgb >> 8) & 255];
	b = p->gtable[rgb & 255];

	//ITU.BT-601 YCbCr 80%
	y  =  r * 0.2989 + g * 0.5866 + b * 0.1145;
	cb = -r * 0.1350 - g * 0.2650 + b * 0.4000;
	cr =  r * 0.4000 - g * 0.3346 - b * 0.0653;

	xx = p->m1.m11 * cb + p->m1.m12 * cr;
	yy = p->m1.m21 * cb + p->m1.m22 * cr;

	dst->z = lround(y  * 16384.0 * 255.0);
	dst->x = lround(xx * 16384.0 * 255.0);
	dst->y = lround(yy * 16384.0 * 255.0);
}

/* GCS->RGB 時、RGB 値の取得 */

static int _get_rgb(double d)
{
	int n;

	d = d / 255 / 16384;

	if(d < 0) d = 0;
	else if(d > 1.0) d = 1.0;

	if(d >= 0.0540269630587776405948631399435028)
		d = pow(d, 2.2/3.0);
	else
		d = pow(d / 0.142913647595774056881018286010431, 2.2);

	n = lround(d * 255);

	if(n < 0) n = 0;
	else if(n > 255) n = 255;

	return n;
}

/** GCS -> RGB */

int colorspace_convGCStoRGB(ColorSpace *p,double x,double y,double z)
{
	double dr,dg,db,xx,yy;
	int r,g,b;

	xx = x, yy = y;

	x = p->m2.m11 * xx + p->m2.m12 * yy;
	y = p->m2.m21 * xx + p->m2.m22 * yy;

	//ITU.BT-601 YCbCr 80%
	dr = z * 0.9998247134019967 + x * -0.000044452970991771824 + y * 1.7528659800326482;
	dg = z * 1.0000893144198046 + x * -0.4320813565838843 + y * -0.89314419804726;
	db = z * 1.0000000115762946 + x * 2.213731098385467 + y * -0.00011576294529099052;

	//RGB

	r = _get_rgb(dr);
	g = _get_rgb(dg);
	b = _get_rgb(db);

	return (r << 16) | (g << 8) | b;
}


