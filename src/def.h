/*$
 Copyright (c) 2021-2022 Azel

 This file is part of quantimg.

 quantimg is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 quantimg is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
$*/

typedef struct _Quant Quant;
typedef struct _ColorSpace ColorSpace;

#define IMAGESIZE_MAX  20000  //画像の最大サイズ

/* フォーマット設定 */

typedef union
{
	struct
	{
		int level;	//0-9
	}png;

	struct
	{
		int level,	//0-9
			quality; //0-100, -1 で可逆
	}webp;
}FormatOpt;

/* アプリデータ */

typedef struct
{
	Quant *quant;		//減色用データ
	ColorSpace *cs;		//色空間データ
	uint8_t *palbuf;	//出力用パレットバッファ (32bit x 256)
						//透過色は終端位置にセットされる。

	mStr str_output;	//出力パス (ファイル名 or ディレクトリ)

	FormatOpt fopt;
	
	int out_format,		//出力形式
		colnum,			//出力色数 (2-256): 透過色含む
		dither_type,	//ディザタイプ
		dither_level;	//ディザ強さ (1-10)
	int32_t opt_tpcol,	//オプションでの透過色指定
		tpcol;			//現在のファイルの透過色 (-1 でなし)
		
	int dstpalnum,		//出力パレット数
		prog_curcnt,	//進捗:現在のカウント数
		prog_curmark,	//進捗:現在のマーク位置
		prog_offset,
		prog_tbl[10];	//進捗:各マーク位置のカウント値
	
	uint8_t	fputmes,	//経過を表示するか
		is_output_dir,	//出力パスがディレクトリか
		is_img_have_tp;	//ソース画像に透過色が含まれるか
}AppData;

extern AppData g_app;

//出力形式
//[!] 順番の変更・追加時: main.c, option.c, image.c
enum
{
	OUTFORMAT_AUTO,	//-o のファイル名から自動判定
	OUTFORMAT_PNG,
	OUTFORMAT_BMP,
	OUTFORMAT_GIF,
	OUTFORMAT_WEBP
};

//ディザタイプ
enum
{
	DITHER_NONE,
	DITHER_ERR,		//誤差拡散
	DITHER_MEAN,	//平均誤差
	DITHER_TONE		//網点(アニメ)
};

/*---------------*/

void exit_app(int ret);
void putmes(const char *fmt,...);
void puterr_exit(const char *fmt,...);
void progress_add(int val,int offpos);

void image_to_palimg(uint8_t **ppbuf,int width,int height);

